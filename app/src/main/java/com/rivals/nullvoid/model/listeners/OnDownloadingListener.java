package com.rivals.nullvoid.model.listeners;

import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * This file is part of PocketMaps
 * <p/>
 * Created by GuoJunjun <rivals.com> on July 09, 2015.
 */
public interface OnDownloadingListener {
    void progressbarReady(TextView downloadStatus, ProgressBar progressBar, int pos);
}
