package com.rivals.nullvoid.model.listeners;

import android.view.View;

/**
 * This file is part of PocketMaps
 * <p/>
 * Created by GuoJunjun <rivals.com> on July 15, 2015.
 */
public interface MapFABonClickListener {
    /**
     * tell Activity what to do when map FAB is clicked
     *
     * @param view
     */
    void mapFABonClick(View view, int pos);
}
